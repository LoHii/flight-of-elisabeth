﻿using UnityEngine;
using System.Collections;

public class DestroyFade : MonoBehaviour {

    public float fadeTime;
    public float delayTime;
    SpriteRenderer rend;
    Color baseColor;
    float percent = 1.0f;
    

    void Start() {
        rend = GetComponent<SpriteRenderer>();
        if (rend == null) {
            Destroy(this);
            Debug.Log("No SpriteRenderer to fade");
        } else
            baseColor = rend.color;
    }

    void Update() {
        if (delayTime == 0)
            percent -= 1 / fadeTime * Time.deltaTime;
        else if (delayTime > 0)
            delayTime = Mathf.Max(delayTime - Time.deltaTime, Mathf.Min(delayTime,0.0f));

        rend.color = new Color(baseColor.r, baseColor.g, baseColor.b, baseColor.a * percent);

        if (percent <= 0)
            Destroy(this.gameObject);
    }
}
