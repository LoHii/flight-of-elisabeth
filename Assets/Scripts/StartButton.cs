﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StartButton : MonoBehaviour {
    public float collect = 0;
    bool holding = false;
    Text text;
    
    public void Up() {
        holding = false;
        text = GetComponent<Text>();
    }

    public void Down() {
        holding = true;
        collect = 0.33f;
        GameManager.current.StartGame();
        transform.parent.gameObject.SetActive(false);
    }
    
	void Update () {
        if (holding) {
            collect += Time.deltaTime*1.22f;

            if (collect >= 1) {
                GameManager.current.StartGame();
                transform.parent.gameObject.SetActive(false);
            }
        } else {
            collect = Mathf.Max(0,collect-Time.deltaTime);
        }

        text.color = new Color(1 - collect, 1, 1 - collect);
    }
}
