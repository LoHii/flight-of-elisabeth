﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public enum Feeling {None, Denial, Anger, Bargaining, Depression, Acceptance};

public class GameManager : MonoBehaviour {
    public Feeling state = Feeling.Denial;
    [HideInInspector]
    public static GameManager current;
    [HideInInspector]
    public DenialManager denial;
    [HideInInspector]
    public AngerManager anger;
    [HideInInspector]
    public BargainingManager bargaining;
    [HideInInspector]
    public DepressionManager depression;
    public GameObject denialPrefab;
    public GameObject angerPrefab;
    public GameObject bargainingPrefab;
    public GameObject depressionPrefab;
    public GameObject backCurtain;
    public float score;
    public BackgroundHandler backgroundHandler;
    public bool counting;
    public bool oneShot = false;
    public GameObject tips;
    public Text end;

    void Start(){
        if (current != null) Debug.LogError("Multiple GameManagers");
        current = this;
        CreateCurtain(Color.white, 0.0f, 1.0f, true);
    }

    void Update(){
        if (counting)
            score += Time.deltaTime;
        if (Time.timeScale == 0.0f && (Input.touches.Length != 0 || Input.anyKeyDown))
            Time.timeScale = 1.0f;
    }

    public static void FadeDestroy(GameObject go, float delayTime, float fadeTime) {
        DestroyFade d = go.AddComponent<DestroyFade>();
        d.delayTime = delayTime;
        d.fadeTime = fadeTime;
    }

    public static void SetScore() {
        current.end.text = "You struggled for " + Mathf.FloorToInt(current.score / 60.0f).ToString() + " minutes and " + (Mathf.FloorToInt(current.score - Mathf.FloorToInt(current.score / 60.0f) * 60)).ToString() + " seconds!";
    }

    public static void CreateCurtain(Color c, float delayTime, float fadeTime, bool onTop) {
        DestroyFade df = Instantiate<GameObject>(current.backCurtain).GetComponent<DestroyFade>();
        df.GetComponent<SpriteRenderer>().color = c;
        df.delayTime = delayTime;
        df.fadeTime = fadeTime;
        if (onTop) {
            df.GetComponent<SpriteRenderer>().sortingOrder *= -1;
            df.transform.position -= 10 * Vector3.forward;
        }
    }

    public static float SlerpLerp(float f) {
        if (f < 0 || f > 1) {
            Debug.Log(f);
            Debug.LogError("Slerpage isn't between [0,1]");
        }

        return Mathf.Clamp((Mathf.Cos(Mathf.Atan((1 - f) / f))),0f,1f);
    }

    public static void SpriteBlack(GameObject go, float delayTime, float fadeTime) {
        if (go.GetComponent<SpriteRenderer>() != null){
            SpriteBlacker sb = go.AddComponent<SpriteBlacker>();
            sb.staySeconds = delayTime;
            sb.fadeSeconds = fadeTime;
        }
    }

    public void CreditStart() {
        SetScore();
        end.transform.parent.gameObject.SetActive(true);
        CameraController.cam.GetComponent<Camera>().backgroundColor = new Color(1,1,1);
    }

    public void StartGame() {
        backgroundHandler.EndBackground();
        CreateCurtain(Color.green, 1.0f, 1.5f, true);
        Invoke("DenialStart", 1.0f);
    }

    public void DenialStart() {
        denial = Instantiate(denialPrefab).GetComponent<DenialManager>();
        state = Feeling.Denial;
        backgroundHandler.StartBackground("Denial", Vector3.right,5);
        denial.transform.SetParent(new GameObject("Denial GameObjects").transform, true);
        tips.SetActive(true);
        tips.transform.FindChild("Denial").GetComponent<TipManager>().Invoke("FadeIn", 2.5f);
        tips.transform.FindChild("Denial").GetComponent<TipManager>().Invoke("FadeOut", 8.5f);
    }

	public void AngerStart(){
        anger = Instantiate(angerPrefab).GetComponent<AngerManager>();
		state = Feeling.Anger;
        backgroundHandler.StartBackground("Anger", Vector3.left,7);
        anger.transform.SetParent(new GameObject("Anger GameObjects").transform, true);
        tips.SetActive(true);
        tips.transform.FindChild("Anger").GetComponent<TipManager>().Invoke("FadeIn", 2.5f);
        tips.transform.FindChild("Anger").GetComponent<TipManager>().Invoke("FadeOut", 11.5f);
    }

    public void BargainingStart() {
        bargaining = Instantiate(bargainingPrefab).GetComponent<BargainingManager>();
        state = Feeling.Bargaining;
        backgroundHandler.StartBackground("Bargaining", Vector3.down,2);
        bargaining.transform.SetParent(new GameObject("Bargaining GameObjects").transform, true);
        tips.SetActive(true);
        tips.transform.FindChild("Bargaining").GetComponent<TipManager>().Invoke("FadeIn", 2.5f);
        tips.transform.FindChild("Bargaining").GetComponent<TipManager>().Invoke("FadeOut", 11.5f);
    }

    public void DepressionStart() {
        depression = Instantiate(depressionPrefab).GetComponent<DepressionManager>();
        state = Feeling.Depression;
        CameraController.cam.GetComponent<Camera>().backgroundColor = new Color(0,0,0);
        depression.transform.SetParent(new GameObject("Depression GameObjects").transform, true);
        tips.SetActive(true);
        tips.transform.FindChild("Depression").GetComponent<TipManager>().Invoke("FadeIn", 2.5f);
        tips.transform.FindChild("Depression").GetComponent<TipManager>().Invoke("FadeOut", 11.5f);
    }

    void Restart() {
        Application.LoadLevel(0);
    }

    public static void End(GameObject holder) {
        current.counting = false;
        Destroy(holder);
        current.backgroundHandler.EndBackground();
        if (current.oneShot) {

        } else {
            switch (current.state) {
                case Feeling.None:
                    GameManager.current.DenialStart();
                    break;
                case Feeling.Denial:
                    GameManager.current.AngerStart();
                    break;
                case Feeling.Anger:
                    GameManager.current.BargainingStart();
                    break;
                case Feeling.Bargaining:
                    GameManager.current.DepressionStart();
                    break;
                case Feeling.Depression:
                    GameManager.current.CreditStart();
                    break;
            }
        }
    }
}
