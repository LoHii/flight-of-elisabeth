﻿using UnityEngine;
using System.Collections;

public class AudioFader : MonoBehaviour {
	public float fadeTime;
	AudioSource aus;
	float startVolume;
	float fadeCollector;

	void Start(){
		aus = GetComponent<AudioSource> ();
		startVolume = aus.volume;
		fadeCollector = fadeTime;
	}

	void Update(){
		fadeCollector -= Time.deltaTime;
		aus.volume = startVolume * (fadeCollector/fadeTime);
        if (fadeCollector <= 0) {
            Destroy(this.gameObject);
        }
	}
}
