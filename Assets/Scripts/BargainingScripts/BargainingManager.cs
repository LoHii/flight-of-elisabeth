﻿using UnityEngine;
using System.Collections;

public class BargainingManager : MonoBehaviour {

    public GameObject elisabethPrefab;
    public GameObject bossPrefab;
    public GameObject sprayPrefab;
    public GameObject wallPrefab;
    BargainingElisabeth bargainingElisabeth;
    GameObject holder;
    int difficulty = 0;
    int lives = 3;
    BargainingBoss boss;
    bool phase;
    bool walls;
    AudioSource loop;

    void Start() {
        bargainingElisabeth = Instantiate(elisabethPrefab).GetComponent<BargainingElisabeth>();
        bargainingElisabeth.transform.SetParent(transform.parent, true);
        Holder();
        Invoke("SpawnBoss", 15.0f);
        loop = AudioLord.PlayLoop("Bargaining Loop");
    }

    void Update() {
        if (phase && holder != null && holder.transform.childCount == 0) {
            phase = false;
            Invoke("SpawnBoss",1.0f);
        }
    }

    void Holder() {
        if (holder != null)
            Destroy(holder);
        holder = new GameObject("Bargaining Holder");
        holder.transform.SetParent(transform.parent);
    }

   public void SpawnBoss() {
        GameManager.current.counting = true;
        if (holder.transform.childCount != 0) return;
        bargainingElisabeth.GetComponent<BoxCollider2D>().enabled = true;
        boss = Instantiate(bossPrefab).GetComponent<BargainingBoss>();
        boss.transform.SetParent(holder.transform, true);
        boss.elisabeth = bargainingElisabeth.transform;
        boss.timerMax = 4.0f + 4.0f * Mathf.Pow(0.66f, difficulty);
        if (difficulty > 3)
            boss.shoot = 3;
    }

    void SpawnPhase() {
        phase = true;
        int side = Random.Range(0, 2) * 2 - 1;
        if (!walls) {
            BargainingSpray bs = Instantiate(sprayPrefab).GetComponent<BargainingSpray>();
            bs.side = side;
            bs.transform.SetParent(holder.transform, true);
            bs.elisabeth = bargainingElisabeth.transform;
            if (difficulty > 5) {
                bs = Instantiate(sprayPrefab).GetComponent<BargainingSpray>();
                bs.side = -1*side;
                bs.transform.SetParent(holder.transform, true);
                bs.elisabeth = bargainingElisabeth.transform;
            }
        } else {
            float speed = (difficulty > 5) ? 2.5f : 5;

            for (int i = 0; i < 5; i++) {
                Transform t = Instantiate(wallPrefab).transform;
                t.position = new Vector3(side * 10.0f, -4 + i * 2, 0);
                t.SetParent(holder.transform, true);
                t.GetComponent<BargainingWall>().startSpeed = speed;
            }

            if (difficulty > 5)
                for (int i = 0; i < 5; i++) {
                    Transform t = Instantiate(wallPrefab).transform;
                    t.position = new Vector3(-side * 10.0f, -4 + i * 2, 0);
                    t.SetParent(holder.transform, true);
                    t.GetComponent<BargainingWall>().startSpeed = speed;
                }
        }
        walls = !walls;
    }

    public void BossDie() {
        AudioLord.Play("Chest Explosion Sound");
        difficulty++;
        Holder();
        CameraController.Shake(0.1f, 0.5f, 0.75f);
        Invoke("SpawnPhase", 1.5f);
    }

    public void End() {
        bargainingElisabeth.GetComponent<BoxCollider2D>().enabled = false;
        Invoke("Holder", 0.75f);
        lives--;
        Handheld.Vibrate();
        if (boss != null) {
            GameManager.FadeDestroy(boss.eyes.GetChild(0).gameObject, 0, 0.75f);
            GameManager.FadeDestroy(boss.eyes.GetChild(1).gameObject, 0, 0.75f);
            Destroy(boss.GetComponent<CircleCollider2D>());
        }

        if (lives > 0) {
            Invoke("SpawnBoss", 3.0f);
            GameManager.CreateCurtain(Color.black, 0.75f, 1.0f, true);
            difficulty = 0;
        } else {
            GameManager.End(transform.parent.gameObject);
            AudioLord.DestroyLoop(loop, 1.0f);
            GameManager.CreateCurtain(Color.black, 1.0f, 1.5f, true);
        }
    }

    void Restart() {
        Application.LoadLevel(0);
    }
}
