﻿using UnityEngine;
using System.Collections;

public class BossBullet : MonoBehaviour {
    public Vector3 target;
    public float speed;

    void OnTriggerEnter2D(Collider2D c) {
        if (c.tag == "Player")
            Destroy(this.gameObject);
    }
	
	void Update () {
        transform.position += target.normalized * speed * Time.deltaTime;

        if (Mathf.Abs(transform.position.x) > 10 || Mathf.Abs(transform.position.y) > 6)
            Destroy(this.gameObject);
    }
}
