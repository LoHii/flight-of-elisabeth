﻿using UnityEngine;
using System.Collections;

public class BargainingElisabeth : MonoBehaviour {

    public GameObject bulletPrefab;
    Vector3 touchPos;
    bool isTouching;
    public float speed;
    public float shootMax;
    float shootCD;
    float minDistance = 2.0f;
    Transform shoot;

	void Start () {
        shoot = transform.GetChild(1);
	}

    void OnTriggerEnter2D(Collider2D c) {
        if (c.tag == "Enemy")
            GameManager.current.bargaining.End();

    }

    void Update () {
	   if (Input.touches.Length != 0) {
            isTouching = true;
            touchPos = CameraController.cam.GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, 10));
        } else {
            isTouching = false;
        }

        if (transform.position.y != touchPos.y) {
            Vector3 v = new Vector3(transform.position.x, Mathf.Clamp(touchPos.y, -4.5f, 4.5f), transform.position.z);

            transform.position = Vector3.MoveTowards(transform.position, v, speed * Time.deltaTime);
        }

        if (isTouching && Mathf.Abs(transform.position.x-touchPos.x) > minDistance && shootCD == 0) {
            shootCD = shootMax;
            BargainingBullet bb = Instantiate(bulletPrefab).GetComponent<BargainingBullet>();
            bb.target = touchPos;
            bb.transform.position = shoot.position;
            bb.transform.SetParent(transform.parent, true);
            if (Mathf.Sign(transform.localScale.x) != Mathf.Sign(touchPos.x - transform.position.x))
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        }

        if (shootCD != 0)
            shootCD = Mathf.Max(shootCD - Time.deltaTime, 0.0f);
    }
}