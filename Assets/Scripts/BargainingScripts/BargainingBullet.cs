﻿using UnityEngine;
using System.Collections;

public class BargainingBullet : MonoBehaviour {

    public Vector3 target;
    public float speed;

	void Start () {
        target = Vector3.Normalize(target - transform.position);
        target = Vector3.RotateTowards(target, transform.position - target, Random.Range(-Mathf.Deg2Rad*5.0f, Mathf.Deg2Rad * 5.0f), 0.0f);
        GetComponentInChildren<Animator>().Play("CoinIdle", -1, Random.value);
	}
	
	void Update () {
        transform.position += target * speed * Time.deltaTime;

        if (Mathf.Abs(transform.position.x) > 10 || Mathf.Abs(transform.position.y) > 5)
            Destroy(this.gameObject);
	}

    void OnTriggerEnter2D(Collider2D c) {
        if (c.tag == "Enemy")
            Destroy(this.gameObject);
    }
}
