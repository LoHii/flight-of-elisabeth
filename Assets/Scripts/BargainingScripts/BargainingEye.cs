﻿using UnityEngine;
using System.Collections;

public class BargainingEye : MonoBehaviour {

    SpriteRenderer rend;
    public float hitSpeed;
    public float hit;

	void Start () {
        rend = GetComponent<SpriteRenderer>();
	}
	
	void Update () {
	    if (hit > 0) {
            hit = Mathf.Max(0, hit - Time.deltaTime * hitSpeed);
            rend.color = Color.Lerp(Color.white, Color.yellow, hit);
        }
	}
}
