﻿using UnityEngine;
using System.Collections;

public class BargainingWall : MonoBehaviour {

    float side;
    public float startSpeed;
    float speed;
    float hit = 0;

	void Start () {
        speed = startSpeed;
        side = Mathf.Sign(transform.position.x);
        transform.Rotate(new Vector3(0, Random.Range(0, 360), 0), Space.World);
	}
	
    void OnTriggerEnter2D(Collider2D c) {
        if (c.name == "Bargaining Bullet(Clone)" && Mathf.Sign(transform.position.x) == side) {
            hit = 0.1f;
        }
    }

    void Update() {
        transform.position += Vector3.left * side * speed * Time.deltaTime;
        transform.Rotate(new Vector3(0, 15 * speed * Time.deltaTime, 0), Space.World);

        if (hit > 0) {
            speed = 0.5f;
            hit = Mathf.Max(0, hit - Time.deltaTime);
        } else if (speed != startSpeed)
            speed = Mathf.Min(startSpeed, speed + 4 * Time.deltaTime);

        if (Mathf.Abs(transform.position.x) > 10.0f && Mathf.Sign(transform.position.x) == -side)
            Destroy(this.gameObject);
    }
}
