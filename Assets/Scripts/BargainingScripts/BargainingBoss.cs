﻿using UnityEngine;
using System.Collections;

public class BargainingBoss : MonoBehaviour {
    public Transform eyes;
    public Transform elisabeth;
    public GameObject bossBullet;
    int life = 30;
    int side = 1;
    bool shrinking;
    float size = 0.0f;
    int difficulty;
    Vector3 startSize;
    Vector3 anchorPos;
    float levitate;
    public float timerMax;
    float timer;
    public int shoot;
    BargainingEye[] eyeRends;
    public ParticleSystem part;
    bool half;

    void Start() {
        side = Random.Range(0, 2) * 2 - 1;
        startSize = transform.localScale;
        HandleSize();
        transform.position = new Vector3((7 - difficulty) * -side, -3f + Random.value * 6f, transform.position.z);
        anchorPos = transform.position;
        timer = timerMax;
        eyes.localPosition = new Vector3(Mathf.Abs(eyes.localPosition.x) * side, eyes.localPosition.y, eyes.localPosition.z);
        eyeRends = eyes.GetComponentsInChildren<BargainingEye>();
        part.Play();
    }

    void Update() {
        HandleSize();
        Levitate();

        if (life <= 0) {
            GameManager.current.bargaining.BossDie();
            part.transform.parent = null;
            part.enableEmission = false;
            Destroy(part.gameObject, part.startLifetime);
        }

        if (size == 1 && !shrinking)
            timer = Mathf.Max(0.0f, timer - Time.deltaTime);

        if (!half && timer < timerMax/2.0f) {
            Shoot();
            half = true;
        }

        if (timer == 0) {
            shrinking = true;
            part.startSize = 0;
            part.enableEmission = false;
            gameObject.tag = "Untaged";
        }
    }

    void OnTriggerEnter2D(Collider2D c) {
        if (c.name == "Bargaining Bullet(Clone)") {
            life--;
            foreach (BargainingEye e in eyeRends)
                e.hit = 1;
        }
    }

    void HandleSize() {
        if (shrinking) {
            size = Mathf.Max(0.0f, size - 3 * Time.deltaTime);
                gameObject.tag = "Enemy";
            if (size == 0)
                Warp();
        } else if (size < 1) {
            size += 3 * Time.deltaTime;
            part.startSize = size*3.5f;
            if (size >= 1) {
                Shoot();
                size = 1;
                gameObject.tag = "Enemy";
                part.startSize = size * 3.5f;
            }
        }
        transform.localScale = size * startSize;
    }

    void Levitate() {
        levitate += Time.deltaTime;
        if (levitate > Mathf.PI)
            levitate -= Mathf.PI * 2;
        transform.position = anchorPos + 0.25f * Vector3.up * Mathf.Sin(levitate) + Vector3.right * (1 - timer / timerMax) * side;
    }

    void Warp() {
        half = false;
        side *= -1;
        difficulty++;
        shrinking = false;
        part.enableEmission = true;
        eyes.localPosition = new Vector3(Mathf.Abs(eyes.localPosition.x) * side, eyes.localPosition.y, eyes.localPosition.z);
        timer = timerMax;

        if (difficulty < 4) {
            anchorPos = new Vector3((7 - difficulty) * -side, -3f + Random.value * 6f, transform.position.z);
        } else
            anchorPos = elisabeth.position;

    }

    void Shoot() {
        if (shoot == 1) {
            BossBullet bb = Instantiate(bossBullet).GetComponent<BossBullet>();
            bb.transform.SetParent(transform.parent, true);
            bb.target = elisabeth.position - anchorPos;
            bb.transform.position = anchorPos;
        } else {
            for (int i = -1; i < 2; i++) {
                BossBullet bc = Instantiate(bossBullet).GetComponent<BossBullet>();
                bc.transform.SetParent(transform.parent, true);
                bc.target = Quaternion.AngleAxis(12 * i, Vector3.forward) * (elisabeth.position - anchorPos);
                bc.transform.position = anchorPos;
                bc.speed *= 3 / 4.0f;
            }
        }
    }
}
