﻿using UnityEngine;
using System.Collections;

public class BargainingSpray : MonoBehaviour {

    float scale = 0;
    public GameObject bullet;
    public Transform elisabeth;
    public int side;
    public float time;
    bool fading;
    ParticleSystem part;
    
	void Start () {
        part = GetComponentInChildren<ParticleSystem>();
        transform.localScale = Vector3.zero;
        Invoke("Shoot", 0.6f);
        transform.position = new Vector3(Mathf.Abs(transform.position.x)*side,transform.position.y, transform.position.z);
	}
	
	void Update () {
        if (scale == 1)
            time -= Time.deltaTime;

        if (!fading && time < 0) {
            fading = true;
            part.enableEmission = false;
            part.transform.parent = null;
            Destroy(part.gameObject, part.startLifetime);
        }


        scale = (fading) ? Mathf.Max(0, scale - 2 * Time.deltaTime) : Mathf.Min(1, scale + Time.deltaTime);
        part.startSize = scale*1.5f;
        transform.localScale = Vector3.one * scale;

        if (scale == 0 && fading)
            Destroy(this.gameObject);
    }

    void Shoot() {
        if (scale == 1) {
            BossBullet bb = Instantiate(bullet).GetComponent<BossBullet>();
            bb.target = Vector3.up * Random.Range(-1.0f, 1.0f) + elisabeth.position - transform.position;
            bb.transform.position = transform.position;
            bb.transform.SetParent(transform.parent, true);
        }
        Invoke("Shoot", 0.5f);
    }
}
