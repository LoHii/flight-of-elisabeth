﻿using UnityEngine;
using System.Collections;

public class BackgroundHandler : MonoBehaviour {

    public bool moving;
    public float speed;
    Transform motion;
    Vector3 dir;
    float yDiff = 12.7019f;

    void Start() {
        motion = transform.GetChild(1);
        StartBackground("Menu", Vector3.right, 1);
    }


    public void StartBackground(string name, Vector3 direction) {
        StartBackground(name, direction, speed);
    }

    public void StartBackground(string name, Vector3 direction, float backSpeed) {
        speed = backSpeed;
        GameObject go = transform.GetChild(0).Find(name).gameObject;
        if (go == null) {
            print("No child found");
            return;
        }
        int amount;
        if (direction == Vector3.zero) {
            amount = 1;
        } else {
            amount = 3;
            moving = true;
        }

        for (int i = 0; i < amount; i++) {
            Transform t = Instantiate(go).transform;
            t.SetParent(motion, false);
            t.position += -i * new Vector3(25.6f * direction.x, direction.y * yDiff, 0.0f);
            for (int j = 0; j < t.childCount; j++)
                t.GetChild(j).gameObject.SetActive(true);
        }
        dir = direction;

        Transform c = transform.GetChild(2).Find(name);
        for (int i = 0; i < c.childCount; i++)
            c.GetChild(i).gameObject.SetActive(true);
    }

    void Update() {
        if (moving)
            MoveBackground();
    }

    public void EndBackground() {

        Transform m = transform.GetChild(1);
        for (int i = 0; i < m.childCount; i++)
            Destroy(m.GetChild(i).gameObject);

        for (int i = 0; i < 5; i++) {
            Transform c = transform.GetChild(2).GetChild(i);
            for (int j = 0; j < c.childCount; j++)
                c.GetChild(j).gameObject.SetActive(false);
        }
    }

    void MoveBackground() {
        for (int i = 0; i < motion.childCount; i++) {
            Transform t = motion.GetChild(i);
            t.position += dir * Time.deltaTime * speed;
            float threshold = 25.0f;
            if (t.position.x * Mathf.Sign(dir.x) > threshold || t.position.y * Mathf.Sign(dir.y) > yDiff)
                t.position -= new Vector3(25.6f * motion.childCount * Sign(dir.x), yDiff * (motion.childCount-1) * Sign(dir.y));
        }
    }

    int Sign(float f) {
        if (f == 0)
            return 0;
        return (int)Mathf.Sign(f);
    }
}
