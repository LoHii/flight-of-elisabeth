﻿using UnityEngine;
using System.Collections;

public class FollowTarget : SeekTarget {
    void Start() {
        speed = maxSpeed;
    }

	void Update () {
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
    }
}
