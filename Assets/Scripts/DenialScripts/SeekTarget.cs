﻿using UnityEngine;
using System.Collections;

public class SeekTarget : MonoBehaviour {

    public float minAngleSpeed;
    public float maxAngleSpeed;
    float angleSpeed;
    public float minSpeed;
    public float maxSpeed;
    [HideInInspector]
    public float speed;
    public float acc;
    float baseAcc;
    float baseMax;
    public Transform target;
    public AnimationCurve curve;
    public bool charging;
    public bool dash = false;
    Quaternion direction;
    bool dashing = true;

    void Start() {
        RaycastHit2D hit = Physics2D.Raycast(Vector2.zero, -target.position, Mathf.Infinity, 1 << 8);
        transform.position = hit.point.normalized * (hit.distance+transform.localScale.x/2.0f);
        Vector3 dir = target.position - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        direction = Quaternion.AngleAxis(angle, Vector3.forward);
        speed = 4;
        charging = true;
        baseAcc = acc;
        baseMax = maxSpeed;
    }

	void Update () {
        if (charging || dash) {
            speed = Mathf.Min(speed + acc * Time.deltaTime, maxSpeed);
            if (!IsPointAhead(target.position)){
                charging = false;
            }
        } else {
            speed = Mathf.Max(speed - acc * Time.deltaTime, minSpeed);
            RaycastHit2D hit = Physics2D.Raycast(transform.position, direction * Vector3.right,Mathf.Infinity, 1 << target.gameObject.layer);
            if (hit.collider != null){
                charging = true;
            }
        }
        angleSpeed = minAngleSpeed + (maxAngleSpeed-minAngleSpeed) * curve.Evaluate((1.0f - (speed - minSpeed) / (maxSpeed-minSpeed)));
        RotateLookTarget(target.position, angleSpeed * Time.deltaTime);
        if (dashing)
            transform.position += direction * Vector3.right * Time.deltaTime * speed;
		acc = baseAcc / baseMax * maxSpeed;

        if (dash && Vector3.Distance(transform.position, Vector3.zero) > 15.0f && dashing) {
            Invoke("Reset", 0.33f);
            dashing = false;
        }
    }

    void Reset() {
        dashing = true;
        RaycastHit2D hit = Physics2D.Raycast(Vector2.zero, Quaternion.AngleAxis(Random.Range(-30.0f,30.0f),Vector3.forward)*(-target.position), Mathf.Infinity, 1 << 8);
        transform.position = hit.point.normalized * (hit.distance + transform.localScale.x / 2.0f);
        RotateLookTarget(target.position, 360.0f);
        speed = minSpeed;
    }

    void RotateLookTarget(Vector3 targetPos, float maxAngle)
    {
        Vector3 dir = targetPos - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        direction = Quaternion.RotateTowards(direction, q, maxAngle);
    }

    bool IsPointAhead(Vector3 point){
        Vector3 forward = direction * Vector3.right;
        Vector3 toOther = point - transform.position;
        if (Vector3.Dot(forward, toOther) < 0)
            return false;
        else
            return true;
    }
}
