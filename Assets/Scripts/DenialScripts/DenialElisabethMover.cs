﻿using UnityEngine;
using System.Collections;

public class DenialElisabethMover: MonoBehaviour {
	Camera cam;
	public Vector3 prevPos;
	BoxCollider2D coll;
	float maxDist = Mathf.Infinity;
	int wallLayer = 1 << 8;
    [HideInInspector]
    public bool canHit = true;
    Animator anim;
    Vector3 prev;

	void Start () {
        cam = CameraController.cam.GetComponent<Camera>();
		coll = GetComponent<BoxCollider2D> ();
        anim = transform.GetComponentInChildren<Animator>();
        prev = transform.position;
	}

    void OnTriggerEnter2D(Collider2D c){
        if (c.tag == "Enemy" && canHit){
            GameManager.current.denial.DenialHit(c.transform.parent.gameObject.GetInstanceID());
            canHit = false;
        }
    } 

	void FixedUpdate () {
		DeltaTouchMove ();
	}

	void DeltaTouchMove() {
		if (Input.touches.Length != 0) {
			if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
			{
				if (Input.touches.Length != 1){
					prevPos = cam.ScreenToWorldPoint(new Vector3(Input.touches[1].position.x,Input.touches[1].position.y,10));
					return;
				}
			}
			
			if (Input.touches[0].phase == TouchPhase.Moved){
				MoveTowardsTarget((cam.ScreenToWorldPoint(new Vector3(Input.touches[0].position.x,Input.touches[0].position.y,10))-prevPos));
				prevPos = cam.ScreenToWorldPoint(new Vector3(Input.touches[0].position.x,Input.touches[0].position.y,10));
			}
			
			if (Input.touches[0].phase == TouchPhase.Began || Input.touches[0].phase == TouchPhase.Stationary){
				prevPos = cam.ScreenToWorldPoint(new Vector3(Input.touches[0].position.x,Input.touches[0].position.y,10));
			}
		}

        if (transform.position.x != prev.x) {
            anim.speed = (transform.position.x > prev.x) ? 0.5f : 2;
            prev = transform.position;
        } else
            anim.speed = 1;

        if (Input.anyKey) {
            Vector3 move = Vector3.zero;
            float speed = 20;

            if (Input.GetKey(KeyCode.W))
                move += Vector3.up;
            if (Input.GetKey(KeyCode.S))
                move += Vector3.down;
            if (Input.GetKey(KeyCode.A))
                move += Vector3.left;
            if (Input.GetKey(KeyCode.D))
                move += Vector3.right;
            MoveTowardsTarget(move.normalized * speed * Time.deltaTime);
        }
    }

	void MoveTowardsTarget(Vector3 target) {
		float max = Mathf.Min (maxDist, target.magnitude);
		float xMax = Mathf.Min (Mathf.Abs(maxDist*target.normalized.x), Mathf.Abs(target.x));
		float yMax = Mathf.Min (Mathf.Abs(maxDist*target.normalized.y), Mathf.Abs(target.y));

		RaycastHit2D ray = Physics2D.BoxCast(coll.offset+(Vector2)transform.position+(Vector2)target.normalized*0.01f, coll.size, 0, (Vector2)target, max,wallLayer);

		if (ray.collider != null) {
			ray = Physics2D.BoxCast(coll.offset+(Vector2)transform.position + new Vector2(target.normalized.x*0.01f,0), coll.size, 0, new Vector2(target.x,0),max,wallLayer);

			if (ray.collider != null)
				xMax = Vector2.Distance (ray.centroid, coll.offset + (Vector2)transform.position + new Vector2(target.normalized.x*0.01f,0));

			ray = Physics2D.BoxCast(coll.offset+(Vector2)transform.position + new Vector2(0, target.normalized.y*0.01f), coll.size, 0, new Vector2(0,target.y),max,wallLayer);
			
			if (ray.collider != null)
				yMax = Vector2.Distance (ray.centroid, coll.offset + (Vector2)transform.position + new Vector2(0, target.normalized.y*0.01f));

			transform.position += new Vector3(target.normalized.x * xMax, target.normalized.y * yMax,0);
		} else
			transform.position += target.normalized * max;
	}
}
