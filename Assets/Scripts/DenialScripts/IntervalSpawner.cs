﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IntervalSpawner: MonoBehaviour {

    public GameObject spawnPrefab;
    public float spawnPerSecond;
    public float aliveTime;
    float spawnTimer;
    [HideInInspector]
    public GameObject spawnParent;
    [HideInInspector]
    public List<GameObject> storeList = new List<GameObject>();

    void Start() {
        spawnParent = new GameObject("Spawn Holder");
        spawnParent.transform.SetParent(GameManager.current.denial.transform.parent, true);
        transform.SetParent(spawnParent.transform, false);
    }

	void Update () {
        spawnTimer += Time.deltaTime;

        if (spawnTimer > 1.0f / spawnPerSecond){
            Spawn();
        }
	}

    public void Spawn() {
        spawnTimer -= 1.0f / spawnPerSecond;
        GameObject go;

        if (storeList.Count == 0)
            go = Instantiate(spawnPrefab);
        else {
            go = storeList[0];
            storeList.RemoveAt(0);
        }

        go.SetActive(true);
        go.transform.position = transform.position;
        go.transform.rotation = transform.rotation;
        go.transform.localScale = new Vector3(spawnPrefab.transform.localScale.x * transform.localScale.x, spawnPrefab.transform.localScale.y * transform.localScale.y, spawnPrefab.transform.localScale.z * transform.localScale.z);
        go.transform.SetParent(spawnParent.transform, false);
        DestroyShrink ds = go.GetComponent<DestroyShrink>();
        
        if (ds != null) {
            ds.spawner = this;
            ds.aliveTime = aliveTime;
        }
    }
}
