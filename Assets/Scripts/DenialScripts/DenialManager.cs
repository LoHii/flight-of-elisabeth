﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class DenialManager : MonoBehaviour {

    [HideInInspector]
    public Transform target;
    public GameObject wallPrefab;
    public float timer = 0.0f;
    public int lives = 3;

    public GameObject denialElisabeth;
    public GameObject firePrefab;
    public GameObject dashPrefab;
    public GameObject followPrefab;
    public float denialSpeedPlus = 0.2f;
    public float denialSpawnMax = 10.0f;
    float spawning = 0.0f;
    int difficulty = 0;
    List<SeekTarget> fireList = new List<SeekTarget>();
    AudioSource loop;

    void Update() {
        if (spawning == 0) {
            timer += Time.deltaTime;

            if (timer > denialSpawnMax) {
                if (fireList.Count != 0)
                    timer -= denialSpawnMax;
                else
                    timer -= denialSpawnMax / 3.0f;
                SpawnDenial();
                GameManager.current.counting = true;
            }
        } else {
            spawning = Mathf.Max(spawning - Time.deltaTime, Mathf.Min(0.0f, spawning));
            if (spawning == 0) {
                if (lives != 0)
                    target.GetComponent<DenialElisabethMover>().canHit = true;
                else
                    Application.LoadLevel(0);
            }
        }
    }

    void Start() {
        timer = denialSpawnMax - 12.0f;
        target = Instantiate(denialElisabeth).transform;
        target.SetParent(transform.parent, true);
        GameObject wall = Instantiate<GameObject>(wallPrefab);
        wall.transform.SetParent(transform.parent, true);
        loop = AudioLord.PlayLoop("Denial Loop");
    }

    void SpawnDenial() {
        SeekTarget st;
        if (difficulty == 0) {
            st = Instantiate(followPrefab).GetComponent<SeekTarget>();
        } else if (difficulty % 2 == 1) {
            st = Instantiate(firePrefab).GetComponent<SeekTarget>();
            st.maxSpeed += 0.3f * difficulty;
            st.minAngleSpeed = +2 * difficulty;
        } else {
            st = Instantiate(dashPrefab).GetComponent<SeekTarget>();
            st.maxSpeed += difficulty * 0.5f;
        }
        st.target = target;
        fireList.Add(st);
        difficulty++;
    }

    public void DenialHit(int id) {
        foreach (SeekTarget st in fireList) {
            Destroy(st.gameObject);
            Destroy(st.GetComponent<IntervalSpawner>().spawnParent);
        }

        difficulty = 0;
        fireList.Clear();
        CameraController.Shake(0.6f, 0.75f, 1.0f);
        Handheld.Vibrate();
        lives--;
        AudioLord.Play("Fire Explosion Sound");

        if (lives != 0) {
            spawning = 3.5f;
            timer = denialSpawnMax;
            GameManager.CreateCurtain(Color.red, 0.75f, 1.0f, true);
        } else {
            Destroy(target.gameObject);
            GameManager.CreateCurtain(Color.red, 1.0f, 1.5f, true);
            AudioLord.DestroyLoop(loop, 1.0f);
            GameManager.End(transform.parent.gameObject);
        }
    }
}
