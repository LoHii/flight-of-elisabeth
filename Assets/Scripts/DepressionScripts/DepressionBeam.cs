﻿using UnityEngine;
using System.Collections;

public class DepressionBeam : MonoBehaviour {

    public Transform target;
    public float speed;
    ParticleSystem part;

	void Start () {
        part = GetComponentInChildren<ParticleSystem>();
	}
    

    void OnTriggerStay2D(Collider2D c) {
        if (c.tag == "Player") {
            Destroy(this.gameObject);
            GameManager.current.depression.AdvancePhase();
            DepressionGem dg = c.GetComponent<DepressionGem>();
            part.enableEmission = false;
            part.transform.parent = null;
            Destroy(part.gameObject, part.startLifetime);
            if (dg != null) 
                dg.GetHit();
        }
    }

	void Update () {
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
	}
}
