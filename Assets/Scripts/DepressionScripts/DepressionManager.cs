﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DepressionManager : MonoBehaviour {

    public GameObject elisabethPrefab;
    public GameObject beamPrefab;
    public GameObject bombPrefab;
    public GameObject minePrefab;
    public DepressionElisabeth depressionElisabeth;
    public enum PhaseType { Beam, Bomb, Mine };
    public PhaseType phase = PhaseType.Beam;
    float birthRadius = 10.0f;
    float timerMax = 3.0f;
    public float timer;
    public float phaseAmount = 0;
    float phaseMax = 5.0f;
    int difficulty;
    float difDelta = 0.7f;
    bool playing = true;
    int lives = 3;
    GameObject holder;
    float safetyValve = -1;
    AudioSource loop;

    void Start() {
        GameManager.current.counting = true;
        depressionElisabeth = Instantiate(elisabethPrefab).GetComponent<DepressionElisabeth>();
        depressionElisabeth.transform.SetParent(transform.parent, true);
        Holder();
        timer = 13.0f;
        loop = AudioLord.PlayLoop("Depression Loop");
    }

    void Update() {
        if (playing) {

            if (safetyValve > 0) {
                safetyValve = Mathf.Max(safetyValve - Time.deltaTime, 0);

                if (safetyValve == 0)
                    PhaseChange();
            }

            if (phaseAmount < phaseMax) {
                timer = Mathf.Max(timer - Time.deltaTime, 0.0f);
                if (timer == 0) {
                    Action();
                }
            }
        }
    }

    void ShootBeam() {
        DepressionBeam db = Instantiate(beamPrefab).GetComponent<DepressionBeam>();
        db.transform.SetParent(holder.transform, true);
        float r = Random.Range(0, 360.0f);
        Vector3 v = Quaternion.AngleAxis(r, Vector3.forward) * new Vector3(birthRadius, 0, 0);
        db.target = depressionElisabeth.transform;
        db.transform.position = db.target.position + v;
        if (difficulty >= 5) {
            //db.speed = 0.5f;
            db = Instantiate(beamPrefab).GetComponent<DepressionBeam>();
            db.transform.SetParent(holder.transform, true);
            v = Quaternion.AngleAxis(r + 120.0f, Vector3.forward) * new Vector3(birthRadius, 0, 0);
            db.target = depressionElisabeth.transform;
            db.transform.position = db.target.position + v;
            //db.speed = 0.5f;
        }
        if (difficulty >= 10) {
            db = Instantiate(beamPrefab).GetComponent<DepressionBeam>();
            db.transform.SetParent(holder.transform, true);
            v = Quaternion.AngleAxis(r - 120.0f, Vector3.forward) * new Vector3(birthRadius, 0, 0);
            db.target = depressionElisabeth.transform;
            db.transform.position = db.target.position + v;
            //db.speed = 0.5f;
        }
    }

    void ShootMine() {
        DepressionMine dm = Instantiate(minePrefab).GetComponent<DepressionMine>();
        dm.transform.SetParent(holder.transform, true);
        float d = Random.Range(depressionElisabeth.minDist, depressionElisabeth.maxDist);
        while (d < depressionElisabeth.distance + 0.15f && d > depressionElisabeth.distance - 0.15f)
            d = Random.Range(depressionElisabeth.minDist, depressionElisabeth.maxDist);
        Vector3 v = Quaternion.AngleAxis(Random.Range(0, 360.0f), Vector3.forward) * new Vector3(d, 0, 0);
        dm.target = depressionElisabeth.transform;
        dm.transform.position = dm.target.position + v;
    }

    void ShootBomb() {
        DepressionBomb db = Instantiate(bombPrefab).GetComponent<DepressionBomb>();
        db.transform.SetParent(holder.transform, true);
        Vector3 v = Quaternion.AngleAxis(Random.Range(0, 360.0f), Vector3.forward) * new Vector3(birthRadius, 0, 0);
        db.target = depressionElisabeth.transform;
        db.transform.position = db.target.position + v;
    }

    public void AdvancePhase() {
        safetyValve = 5.0f;
        if (phaseAmount >= phaseMax && holder.transform.childCount <= 1) {
            Invoke("PhaseChange", 0.5f);
            Debug.Log("Change");
        }

    }

    void PhaseChange() {
        int next = Mathf.RoundToInt(Random.value);
        phaseAmount = 0;
        difficulty++;
        phaseMax = 1.0f + 4.0f / Mathf.Pow(difDelta, difficulty);
        timer = 0;
        safetyValve = -1.0f;

        if (phase == PhaseType.Beam) {
            phase = (next == 0) ? PhaseType.Bomb : PhaseType.Mine;
        } else if (phase == PhaseType.Bomb) {
            phase = (next == 0) ? PhaseType.Beam : PhaseType.Mine;
        } else {
            phase = (next == 0) ? PhaseType.Beam : PhaseType.Bomb;
        }
    }

    void Action() {
        if (phase == PhaseType.Beam) {
            phaseAmount += 0.5f;
            ShootBeam();
            timer = 0.125f + (timerMax - 0.125f) / 2f * Mathf.Pow(difDelta, difficulty);
            if (difficulty >= 5)
                timer *= 2;
        } else if (phase == PhaseType.Bomb) {
            phaseAmount += 0.75f;
            ShootBomb();
            timer = 0.1875f + (timerMax-0.1875f) * 3f/4f * Mathf.Pow(difDelta, difficulty);
        } else {
            phaseAmount += 1f;
            ShootMine();
            timer = 0.25f + (timerMax-0.25f) * Mathf.Pow(difDelta, difficulty);
        }
    }

    public void End() {
        Handheld.Vibrate();
        lives--;
        playing = false;
        safetyValve = -1.0f;
        if (lives > 0) {
            difficulty = 0;
            phaseAmount = 0f;
            phaseMax = 5.0f;
            Invoke("PlayAgain", 2.5f);
            GameManager.CreateCurtain(Color.white, 0.75f, 1.0f, true);
            depressionElisabeth.Reset();
            Holder();
        } else {
            difficulty = 0;
            AudioLord.DestroyLoop(loop, 1.0f);
            GameManager.CreateCurtain(Color.white, 1.0f, 1.5f, true);
            GameManager.End(transform.parent.gameObject);
        }
    }

    void PlayAgain() {
        playing = true;
    }

    void Restart() {
        Application.LoadLevel(0);
    }

    void Holder() {
        if (holder != null)
            Destroy(holder);
        holder = new GameObject("Depression Holder");
        holder.transform.SetParent(transform.parent, true);
    }
}
