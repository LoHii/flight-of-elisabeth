﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DepressionElisabeth : MonoBehaviour {

    public GameObject specialPrefab;
    public GameObject normalPrefab;
    float targetDistance = 3.0f;
    [HideInInspector]
    public float distance;
    public float targetAngle = 0.0f;
    float angle;
    public float maxDist = 4f;
    public float minDist = 1.5f;
    public float drift;
    float driftDecc = 10.0f;
    bool touching;
    Vector3 prevPos;
    float prevDist;
    public float distanceDelta;
    public int specialGems;
    public int normalPerSpecial;
    List<GemStruct> gemList = new List<GemStruct>();
    GameObject gemHolder;
    Camera cam;
    public float rotSpeed;
    Transform model;

    struct GemStruct {public GameObject gem; public float angleDifference; public DepressionGem dg;
        public GemStruct (GameObject gem, float angleDifference) {
            this.gem = gem;
            this.angleDifference = angleDifference;
            this.dg = gem.GetComponent<DepressionGem>();
        }
    };

	void Start () {
        CreateGems();
        cam = CameraController.cam.GetComponent<Camera>();
        model = transform.GetChild(0);

	}
	
	void Update () {
        MoveRing();
        model.Rotate(Vector3.up * rotSpeed * Time.deltaTime, Space.World);
	}

    float ClampEuler(float f) {
        while (f < -360.0f || f >= 360.0f)
            f = f - Mathf.Sign(f) * 360.0f;

        return f;
    }

    void MoveRing() {
        if (Input.touches.Length != 0) {
            if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled) {
                if (Input.touches.Length > 1) {
                    prevPos = cam.ScreenToWorldPoint(new Vector3(Input.touches[1].position.x, Input.touches[1].position.y, 10));
                    touching = true;
                } else {
                    touching = false;
                }
            }


            if (Input.touches.Length >= 2){ 
                if (Input.touches[1].phase == TouchPhase.Began || Input.touches[1].phase == TouchPhase.Stationary || Input.touches[0].phase == TouchPhase.Began || Input.touches[0].phase == TouchPhase.Stationary) {
                    prevDist = Vector3.Distance(cam.ScreenToWorldPoint(new Vector3(Input.touches[1].position.x, Input.touches[1].position.y, 10)), cam.ScreenToWorldPoint(new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, 10)));
                } else if(Input.touches[0].phase == TouchPhase.Moved || Input.touches[1].phase == TouchPhase.Moved) {
                    float newDist = Vector3.Distance(cam.ScreenToWorldPoint(new Vector3(Input.touches[1].position.x, Input.touches[1].position.y, 10)), cam.ScreenToWorldPoint(new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, 10)));
                    targetDistance = Mathf.Clamp(targetDistance + (newDist - prevDist)/1.4f, minDist, maxDist);
                    prevDist = newDist;
                }
            }

            if (Input.touches[0].phase == TouchPhase.Moved) {
                Vector3 v = cam.ScreenToWorldPoint(new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, 10));
                if (Vector3.Distance(v, transform.position) > 0.75f) {
                    drift = targetAngle;
                    targetAngle += CheckAngle(prevPos, v);
                    drift = targetAngle - drift;
                    if (drift > 300)
                        drift -= 360;

                    drift = Mathf.Clamp(drift, -15.0f, 15.0f);
                    targetAngle = ClampEuler(targetAngle);
                    prevPos = v;
                }
            }

            if (Input.touches[0].phase == TouchPhase.Began || Input.touches[0].phase == TouchPhase.Stationary) {
                prevPos = cam.ScreenToWorldPoint(new Vector3(Input.touches[0].position.x, Input.touches[0].position.y, 10));
                touching = true;
            }
        }

        if (targetAngle != angle || targetDistance != distance || drift != 0) {
            if (touching)
                angle = targetAngle;
            else if (drift != 0) {
                angle += drift;
                targetAngle += drift;
                drift = (Mathf.Sign(drift) == -1) ? Mathf.Min(0.0f, drift + Time.deltaTime * driftDecc) : Mathf.Max(0.0f, drift - Time.deltaTime * driftDecc);
            }
            distance = targetDistance;

            foreach (GemStruct gs in gemList) {
                OrientGem(gs);
            }
        }
    }

    void CreateGems() {
        float totalGems = (float)specialGems + specialGems * normalPerSpecial;
        distance = targetDistance;
        angle = targetAngle;
        if (gemHolder != null)
            Destroy(gemHolder);
        gemHolder = new GameObject("Gem Holder");
        gemHolder.transform.SetParent(transform.parent, true);

        for (int i = 0; i < (int)totalGems; i++) {
            GameObject gem;
            float diff = 360.0f / totalGems * i;

            if (i * specialGems / totalGems == Mathf.FloorToInt(i * specialGems / totalGems)) {
                gem = specialPrefab;
            } else {
                gem = normalPrefab;
            }
            gem = Instantiate(gem);
            GemStruct gs = new GemStruct(gem, diff);
            OrientGem(gs);
            gem.transform.SetParent(gemHolder.transform, false);
            gemList.Add(gs);
        }
    }

    float CheckAngle(Vector3 from, Vector3 to) {

        float ang = Vector3.Angle(from, to);
        Vector3 cross = Vector3.Cross(from, to);

        if (cross.z < 0)
            ang = 360.0f - ang;

        return ClampEuler(ang);
    }

    public void DestroyGem(GameObject go) {
        int index = gemList.FindIndex(x => x.gem == go);
        DestroyGem(index);
    }

    public void DestroyGem(int index) {
        Destroy(gemList[index].gem);
        gemList.RemoveAt(index);
    }

    public void OnTriggerEnter2D(Collider2D c) {
        GameManager.current.depression.End();
    }

    public void Reset() {
        for (int i = 0; i < gemList.Count; i++) {
            Destroy(gemList[i].gem);
        }
        gemList.Clear();
        CreateGems();
    }

    void OrientGem(GemStruct gs) {
        GameObject gem = gs.gem;
        float diff = gs.angleDifference;
        gem.transform.position = transform.position + Quaternion.AngleAxis(ClampEuler(diff + angle), Vector3.forward) * Vector3.right * distance;
        //gem.transform.rotation = Quaternion.Euler(new Vector3(0, 0, ClampEuler(45.0f + diff + angle)));
    }
}
