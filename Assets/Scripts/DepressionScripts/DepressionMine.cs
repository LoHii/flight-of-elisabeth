﻿using UnityEngine;
using System.Collections;

public class DepressionMine : MonoBehaviour {

    public float growDelta;
    float grow = 0;
    Vector3 startPos;
    bool ready;
    public Transform target;
    public float speed;
    ParticleSystem part;

    void Start() {
        transform.localScale = new Vector3(grow, grow, grow);
        startPos = transform.position;
        part = GetComponentInChildren<ParticleSystem>();
    }

    void OnTriggerStay2D(Collider2D c) {
        DepressionElisabeth e = c.GetComponent<DepressionElisabeth>();
        if (e != null) {
            Destroy(this.gameObject);
        } else if (c.tag == "Player" && !ready) {
            Destroy(this.gameObject);
            GameManager.current.depression.AdvancePhase();
            DepressionGem dg = c.GetComponent<DepressionGem>();
            part.enableEmission = false;
            part.transform.parent = null;
            Destroy(part.gameObject, part.startLifetime);
            if (dg != null)
                dg.GetHit();
        }
    }

    void Update() {
        if (!ready) {
            grow = Mathf.Min(grow + growDelta * Time.deltaTime, 1.0f);
            transform.localScale = new Vector3(grow, grow, grow);
            if (grow > 0.5f)
                transform.position = startPos + new Vector3(Random.value * (0.4f * grow), Random.value * (0.4f * grow), 0);

            if (grow == 1.0f) {
                transform.position = startPos;
                ready = true;
            }
            part.startSize = 0.6f * grow;
        } else {
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
    }
}
