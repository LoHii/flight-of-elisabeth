﻿using UnityEngine;
using System.Collections;

public class DepressionBomb : MonoBehaviour {

    public Transform target;
    public float speed;
    public float radius;
    public GameObject boomPrefab;
    Transform orbiter;
    float orbiterAngle;
    float orbiterSpeed = 240.0f;

	void Start () {
        orbiter = transform.Find("Orbiter");
        orbiterAngle = Random.Range(0, 360);
        orbiter.localPosition = Quaternion.AngleAxis(orbiterAngle, Vector3.forward) * new Vector3(radius, 0, 0);
	}
	
    void OnTriggerStay2D(Collider2D c) {
        bool hit = false;
        if (c.tag == "Player") {
            Destroy(this.gameObject);

            Collider2D[] cArray = Physics2D.OverlapCircleAll(transform.position, radius, 1 << 9);
            foreach (Collider2D coll in cArray) {
                DepressionGem n = coll.GetComponent<DepressionGem>();
                if (n != null)
                    n.GetHit();
                else {
                    DepressionElisabeth e = coll.GetComponent<DepressionElisabeth>();
                    if (e != null) {
                        e.OnTriggerEnter2D(GetComponent<Collider2D>());
                        hit = true;
                    }
                    break;
                }
            }

            GameManager.current.depression.AdvancePhase();
            GameObject go = Instantiate(boomPrefab);
            go.transform.localScale = new Vector3(radius*2, radius*2, 1);
            go.transform.position = transform.position + Vector3.back*5;

            if (hit)
                go.GetComponent<SpriteRenderer>().color = Color.black;
        }
    }

	void Update () {
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        orbiterAngle += orbiterSpeed * Time.deltaTime;
        if (orbiterAngle < 0 || orbiterAngle >= 360)
            orbiterAngle += -360.0f * Mathf.Sign(orbiterAngle);
        orbiter.localPosition = Quaternion.AngleAxis(orbiterAngle, Vector3.forward) * new Vector3(radius, 0, 0);
    }
}
