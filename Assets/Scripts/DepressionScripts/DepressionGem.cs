﻿using UnityEngine;
using System.Collections;

public class DepressionGem : MonoBehaviour {

    public bool unbreakable;
    public float size;
    public float targetSize;
    Vector3 orig;
    Vector3 prev;
    bool canHit;
    Transform model;
    float pitch;

    void Start() {
        canHit = true;
        orig = transform.localScale;
        targetSize = 1;
        size = 1;
        model = transform.GetChild(0);
        model.Rotate(Vector3.forward, Random.Range(0, 360));
        prev = transform.position;
        pitch = Random.Range(0.7f, 1.3f)*(Random.Range(0,2)*2-1);
    }

	public void GetHit() {
        if (!unbreakable && canHit) {
            GameManager.current.depression.depressionElisabeth.DestroyGem(this.gameObject);
        }
    }

    void Update() {
        transform.localScale = orig * size;
        model.Rotate(Vector3.forward, Time.deltaTime*30*pitch);

        if (transform.position == prev) {
            if (targetSize != 1)
                targetSize = Mathf.Min(1, targetSize + Time.deltaTime);
        } else {
            float angle = Vector3.Angle(prev, transform.position);
            if (angle < 4f) {
                targetSize = 1 - angle / 4f * 0.2f;
            } else {
                targetSize = 0.2f;
                canHit = false;
                gameObject.tag = "Untagged";
            }
            prev = transform.position;
        }

        if (size < 0.8f && canHit) {
            canHit = false;
            gameObject.tag = "Untagged";
        }
        if (size > 0.8f && !canHit) {
            canHit = true;
            gameObject.tag = "Player";
        }

        if (size > targetSize)
            size = Mathf.Max(targetSize, size - 2 * Time.deltaTime);
        if (size < targetSize)
            size = Mathf.Min(targetSize, size + 2 * Time.deltaTime);

    }
}
