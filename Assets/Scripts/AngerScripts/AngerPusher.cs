﻿using UnityEngine;
using System.Collections;

public class AngerPusher : MonoBehaviour {

    public float speed;
    public Material breakMaterial;
    float pushSpeed;
    bool hasHit;
    bool broken;
    AngerElisabeth ae;
    public int shake;
    public int shakeLimit;
    Vector3 shakeVector;
    MeshRenderer rend;
    public float size;
    Color og;
    bool opening;
    Animator anim;
    Transform model;
    float regSpeed;

    void Start() {
        model = transform.GetChild(0);
        anim = model.GetChild(1).GetComponent<Animator>();
        pushSpeed = 1.25f;
        rend = model.GetChild(0).GetChild(0).GetComponent<MeshRenderer>();
        size = GetComponent<BoxCollider2D>().size.x * transform.localScale.x / 2;
        og = rend.material.color;
    }

    void OnTriggerEnter2D(Collider2D coll) {
        if (coll.tag == "Player" && !hasHit) {
            ae = coll.GetComponent<AngerElisabeth>();
            ae.SetMove(false);
            hasHit = true;
            regSpeed = speed;
            speed = pushSpeed;
            ae.animMultiplier = 3;
            CameraController.Zoom(2.5f, new Vector2(-2.5f * 16 / 9, transform.position.y), 1.0f);
        }
    }

    void Update() {
        if (!opening) {
            model.transform.Rotate(Vector3.right, 18 * speed * Time.deltaTime);
        } else
            model.transform.rotation = Quaternion.Lerp(model.transform.rotation, Quaternion.Euler(new Vector3(0, 189, 0)), Time.deltaTime * 5);

        if (!hasHit || broken) {
            transform.position += Vector3.left * (speed * Time.deltaTime);
        } else {
            transform.position += Vector3.left * (pushSpeed * Time.deltaTime);
            ae.pushed = ae.startPos.x - transform.position.x + size + 0.5f;
            if (Input.acceleration != Vector3.zero) {
                Vector3 v = Input.acceleration;
                if (shakeVector == Vector3.zero || Mathf.Abs(shakeVector.magnitude - v.magnitude) >= 0.35f)
                    shake++;

                if (shake == shakeLimit) {
                    Kill();
                }

                rend.material.color = Color.Lerp(og, Color.black, shake / (float)shakeLimit);
                shakeVector = v;
            }
        }

        if ((hasHit || broken) && transform.position.x < -10) {
            if (!broken)
                End();
            Destroy(this.gameObject);
        }

        if (!hasHit && !opening && transform.position.x < -7.5f) {
            GameManager.current.anger.StopChest();
            model.GetChild(0).gameObject.SetActive(false);
            model.GetChild(1).gameObject.SetActive(true);
            Invoke("End", 1.0f);
            anim.Play("Open");
            CameraController.Zoom(1.25f, transform.position, 1.0f);
            opening = true;
        }
    }

    void Kill() {
        Break();
        GameManager.current.anger.PushKill();
    }

    void End() {
        GameManager.current.anger.End();
    }

    void Break() {
        CameraController.Zoom(5.0f, Vector2.zero, 0.5f);
        ae.SetMove(true);
        ae.animMultiplier = 1;
        broken = true;
        speed = 2 * regSpeed;
        Destroy(GetComponent<BoxCollider2D>());
        model.GetChild(0).gameObject.SetActive(false);
        model.GetChild(1).gameObject.SetActive(true);
        MeshRenderer[] rends = model.GetChild(1).GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer mr in rends)
            mr.material.CopyPropertiesFromMaterial(breakMaterial);
        anim.Play("Break");
        rend.material.color = og;
    }

    void OnDestroy() {
        Resources.UnloadUnusedAssets();
    }
}
