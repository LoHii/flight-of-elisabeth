﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AngerManager : MonoBehaviour {

    public GameObject elisabethPrefab;
    AngerElisabeth angerElisabeth;
    public GameObject tutorialPrefab;
    public GameObject chestPrefab;
    public GameObject pushPrefab;
    GameObject chestHolder;
    public static int lanes = 4;
    float timer;
    public float spawnTime;
    float spawnStart;
    GameObject[] tutArray;
    Camera cam;
    bool tutOver;
    enum EndState { Playing, Reset, End};
    EndState end;
    int lives = 3;
    int prev = -1;
    int spawnCount;
    public int spawnMax;
    int countMax;
    int difficulty;
    AudioSource loop;

    void Start() {
        angerElisabeth = Instantiate(elisabethPrefab).GetComponent<AngerElisabeth>();
        angerElisabeth.transform.SetParent(transform.parent, true);
        tutArray = new GameObject[lanes];
        timer = spawnTime + 15.0f;
        spawnStart = spawnTime;
        countMax = spawnMax;
        chestHolder = new GameObject("Chest Holder");
        chestHolder.transform.SetParent(transform.parent, true);
        loop = AudioLord.PlayLoop("Anger Loop");
    }

    void Update() {
        if (tutOver && end == EndState.Playing)
            timer -= Time.deltaTime;
        else
            CheckTutorial();

        if (timer < 0) {
            GameManager.current.counting = true;
            spawnCount++;
            timer += spawnTime;
            int next = Random.Range(0, lanes);
            while (next == prev)
                next = Random.Range(0, lanes);
            prev = next;
            if (spawnCount < spawnMax) {
                Transform t = Instantiate(chestPrefab).transform;
                t.position = new Vector3(t.position.x, LaneY(next), t.position.z);
                t.SetParent(chestHolder.transform, false);
                t.GetComponent<AngerChest>().speed *= spawnStart / spawnTime;
            } else if (spawnCount == spawnMax) {
                Transform t = Instantiate(pushPrefab).transform;
                t.position = new Vector3(t.position.x, LaneY(next), t.position.z);
                t.SetParent(chestHolder.transform, false);
                t.GetComponent<AngerPusher>().speed *= spawnStart / spawnTime;
            }
        }
    }


    public static float LaneY(int lane) {
        Camera cam = CameraController.cam.GetComponent<Camera>();

        float f = cam.orthographicSize * 2 / lanes * lane + cam.orthographicSize / lanes - cam.orthographicSize;
        return f;
    }

    public void StopChest() {
        for (int i = 0; i < chestHolder.transform.childCount; i++) {
            AngerChest ac = chestHolder.transform.GetChild(i).GetComponent<AngerChest>();
            if (ac != null) {
                ac.speed = 0;
            }

            AngerPusher ap = chestHolder.transform.GetChild(i).GetComponent<AngerPusher>();
            if (ap != null) {
                ap.speed = 0;
            }
        }
        tutOver = false;
        angerElisabeth.SetMove(false);
    }

    void CheckTutorial() {
        foreach (GameObject go in tutArray)
            if (go != null)
                return;

        tutOver = true;
    }

    public void End() {
        if (end == EndState.Playing) {
            for (int i = 0; i < chestHolder.transform.childCount; i++) {
                Transform c = chestHolder.transform.GetChild(i);
                Destroy(c.gameObject);
            }
            lives--;
            tutOver = true;
            AudioLord.Play("Chest Chime Sound");
            Handheld.Vibrate();
            CameraController.Zoom(5.0f, Vector2.zero, 0.5f);
            angerElisabeth.SetMove(true);
            angerElisabeth.animMultiplier = 1;
            difficulty = 0;

            if (lives > 0) {
                Invoke("PlayAgain", 3.0f);
                end = EndState.Reset;
                GameManager.CreateCurtain(Color.yellow,0.75f,1.0f,true);
                spawnTime = spawnStart;
                spawnMax = countMax;
                spawnCount = 0;
            } else {
                end = EndState.End;
                AudioLord.DestroyLoop(loop, 1.0f);
                GameManager.CreateCurtain(Color.yellow, 1.0f, 1.5f, true);
                GameManager.End(transform.parent.gameObject);
            }
        }
    }

    public void PlayAgain() {
        end = EndState.Playing;
    }

    public void PushKill() {
        difficulty++;
        CameraController.Shake(0.5f, 1.0f, 0.75f);
        Invoke("PlayAgain", 2.0f);
        end = EndState.Reset;
        float escalation = (difficulty < 3) ? 0.6f: 0.8f;
        spawnTime = (spawnTime - angerElisabeth.GetComponent<AngerElisabeth>().chargeDuration) * escalation + angerElisabeth.GetComponent<AngerElisabeth>().chargeDuration;
        angerElisabeth.speed *= spawnStart / spawnTime;
        spawnCount = 0;
        spawnMax += countMax/2;
    }

    void Restart() {
        Application.LoadLevel(0);
    }
}
