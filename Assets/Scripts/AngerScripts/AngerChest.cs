﻿using UnityEngine;
using System.Collections;

public class AngerChest : MonoBehaviour {

    public float speed;
    public bool hit;
    bool opening;
    public Animator anim;
    Transform model;
    float pitch;

    void Start() {
        model = transform.GetChild(0);
        anim = model.GetChild(1).GetComponent<Animator>();
        model.transform.Rotate(Vector3.right, Random.Range(0,360));
        pitch = Random.Range(0.8f, 1.25f);
    }

    void Update() {
        transform.position += Vector3.right * (-speed*Time.deltaTime);

        if (!opening) {
                model.transform.Rotate(Vector3.right, 18 * speed * pitch * Time.deltaTime);
        } else
            model.transform.rotation = Quaternion.Lerp(model.transform.rotation, Quaternion.Euler(new Vector3(0, 189, 0)), Time.deltaTime*5);

        if (!hit && transform.position.x < -7.5f) {
            if (!opening) {
                GameManager.current.anger.StopChest();
                model.GetChild(0).gameObject.SetActive(false);
                model.GetChild(1).gameObject.SetActive(true);
                Invoke("End", 1.0f);
                anim.Play("Open");
                CameraController.Zoom(1.25f, transform.position, 1.0f);
                opening = true;
            }
        } else if (hit && transform.position.x < -10.0f) {
            Destroy(this.gameObject);
        }
    }

    void End() {
        GameManager.current.anger.End();
    }

    public void Hit() {
        hit = true;
        speed *= 2;
        Destroy(GetComponent<BoxCollider2D>());
        model.GetChild(0).gameObject.SetActive(false);
        model.GetChild(1).gameObject.SetActive(true);
        anim.Play("Break");
    }
}
