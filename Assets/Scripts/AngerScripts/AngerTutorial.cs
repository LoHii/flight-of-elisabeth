﻿using UnityEngine;
using System.Collections;

public class AngerTutorial : MonoBehaviour {

    Vector3 targetPos;
    public float speed;

	void Start () {
        targetPos = new Vector3(-2.0f, transform.position.y, transform.position.z);
	}
	
	void Update () {
        transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
	}
}
