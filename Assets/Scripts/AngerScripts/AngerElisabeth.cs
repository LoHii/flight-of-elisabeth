﻿using UnityEngine;
using System.Collections;

public class AngerElisabeth : MonoBehaviour {

    Vector3 targetPos;
    [HideInInspector]
    public Vector3 startPos;
    Camera cam;
    float chargeTime = 0;
    public float chargeDuration;
    int currentLane;
    bool tryingToCharge;
    SkinnedMeshRenderer rend;
    Color startCol;
    int targetLane = -1;
    public float speed;
    float hitTime;
    float hitDuration;
    [HideInInspector]
    public float pushed = 0;
    bool canMove = true;
    Animator anim;
    public float animMultiplier = 1;
    float animSpeed;

    void Start() {
        cam = CameraController.cam.GetComponent<Camera>();
        currentLane = Mathf.FloorToInt((AngerManager.lanes - 1) / 2.0f);
        startPos = transform.position;
        transform.position = new Vector3(startPos.x, AngerManager.LaneY(currentLane), startPos.z);
        targetPos = transform.position + new Vector3(pushed,0,0);
        rend = GetComponentInChildren<SkinnedMeshRenderer>();
        startCol = rend.material.color;
        targetLane = currentLane;
        anim = transform.GetChild(0).GetComponent<Animator>();
        animSpeed = anim.speed;
        hitDuration = chargeDuration;
    }

    void Update() {
        anim.speed = animMultiplier * animSpeed;

        if (canMove) {
            TapMove();

            pushed = Mathf.Max(pushed - 5.0f * Time.deltaTime, 0);
        }

        if (transform.position != targetPos) {
            transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
        }

        targetPos = new Vector3(startPos.x - pushed, targetPos.y, targetPos.z);

        if (targetLane != -1 && chargeTime == 0 && !tryingToCharge) {
            currentLane = targetLane;
            targetLane = -1;
            targetPos = new Vector3(startPos.x - pushed, AngerManager.LaneY(currentLane), startPos.z);
            tryingToCharge = true;
        }


        if (tryingToCharge && WithinLane(currentLane)) {
            chargeTime = chargeDuration;
            tryingToCharge = false;
        }

        if (chargeTime > 0) {
            chargeTime = Mathf.Max(0, chargeTime - Time.deltaTime);
        }

        if (hitTime > 0) {
            hitTime = Mathf.Max(0, hitTime - Time.deltaTime);
            rend.material.color = Color.Lerp(startCol, Color.red, hitTime / hitDuration);
            if (hitTime != 0)
                animMultiplier = 2;
            else
                animMultiplier = 1;
        }
    }

    void OnTriggerEnter2D(Collider2D c) {
        if (c.tag == "Enemy") {
            c.GetComponent<AngerChest>().Hit();
            hitTime = hitDuration;
            AudioLord.Play("Chest Explosion Sound");
            CameraController.Shake(0.1f, 0.2f, 0.2f);
        }
    }

    void TapMove() {
        if (Input.touches.Length != 0) {
            if (Input.touches[0].phase == TouchPhase.Began) {
                int test = Mathf.FloorToInt(Input.touches[0].position.y * AngerManager.lanes / (cam.pixelHeight + 1));

                if (test != currentLane)
                    targetLane = test;
            }
        }

        if (Input.anyKeyDown) {
			int test = -1;

			if (Input.GetKey(KeyCode.A))
				test = 0;

            if (Input.GetKey(KeyCode.W))
				test = 1;

            if (Input.GetKey(KeyCode.S))
				test = 2;

			if (Input.GetKey(KeyCode.D))
				test = 3;

			if (test != -1)
            targetLane = Mathf.Clamp(test, 0, AngerManager.lanes - 1);
        }
    }

    bool WithinLane(int lane) {
        Camera cam = CameraController.cam.GetComponent<Camera>();

        if (transform.position.y <= AngerManager.LaneY(lane) + cam.orthographicSize / AngerManager.lanes && transform.position.y >= AngerManager.LaneY(lane) - cam.orthographicSize / AngerManager.lanes)
            return true;

        return false;
    }

    public void SetMove(bool can) {
        if (can)
            canMove = true;
        else {
            canMove = false;
            currentLane = Mathf.FloorToInt((transform.position.y+ cam.orthographicSize) * AngerManager.lanes / (cam.orthographicSize*2+0.01f));
            targetLane = -1;
            targetPos = new Vector3(startPos.x - pushed, AngerManager.LaneY(currentLane), startPos.z);
            tryingToCharge = true;
        }
    }
}