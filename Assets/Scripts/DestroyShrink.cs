﻿using UnityEngine;
using System.Collections;

public class DestroyShrink : MonoBehaviour {

    public float aliveTime;
    Vector3 baseScale;
    float percent = 1.0f;
    SpriteRenderer rend;
    public IntervalSpawner spawner;

    void Start() {
        baseScale = transform.localScale;
        rend = GetComponent<SpriteRenderer>();
    }

    void Update() {
        percent -= 1 / aliveTime * Time.deltaTime;
        rend.sortingOrder = -100 + Mathf.CeilToInt(transform.localScale.x * 100);
        transform.localScale = baseScale * percent;

        if (percent <= 0) { 
            if (spawner != null)
                Destroy(this.gameObject);
            else {
                gameObject.SetActive(false);
                percent = 1.0f;
                spawner.storeList.Add(this.gameObject);
            }
        }
	}
}
