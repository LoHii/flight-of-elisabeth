﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TipManager : MonoBehaviour {

    Image black;
    Text text;
    bool more;
    bool once;
    float alpha;
    float blackStart = 0.75f;
    float speed = 1.0f / 2.5f;

	void Start () {
        text = GetComponentInChildren<Text>();
        black = GetComponentInChildren<Image>();
	}
	
    public void FadeIn() {
        once = true;
        more = true;
    }

    public void FadeOut() {
        more = false;
    }

    void Update () {
        if (!more) {
            alpha = Mathf.Max(0, alpha - Time.deltaTime * speed);
            if (once && alpha == 0) {
                transform.parent.gameObject.SetActive(false);
                once = false;
            }
        } else {

            alpha = Mathf.Min(1, alpha + Time.deltaTime * speed);
        }

        black.color = Color.black * blackStart * alpha;
        text.color = Color.white * alpha;
	}
}
