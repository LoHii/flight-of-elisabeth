﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {


	public static GameObject cam;
	float shakeFade;
	float shakeAmount;
	float shakeMaxDuration;
	Vector3 shakeVector;
	float shake = 0;
	float lerper;
    Camera camComp;

    Vector3 zoomStartPos;
    Vector3 zoomEndPos;
    float zoomEndSize;
    float zoomStartSize;
    float zoomLerp;
    float zoomSecond;

	void Awake () {
		if (cam != null)
			Debug.LogError ("Multiple Shakers");
		cam = gameObject;
        camComp = GetComponent<Camera>();
        zoomEndPos = transform.position;
        zoomEndSize = camComp.orthographicSize;
	}

	public static void Shake (float shakeAmount, float shakeDuration, float shakeFade){
		cam.GetComponent<CameraController>().StartShaking(shakeAmount, shakeDuration, shakeFade);
	}

    public static void Zoom(float screenSize, Vector2 endPos, float zoomDuration) {
        cam.GetComponent<CameraController>().StartZooming(screenSize, endPos, zoomDuration);
    }

    public void StartZooming(float sS, Vector2 eP, float zD) {
        zoomStartPos = zoomEndPos;
        zoomStartSize = zoomEndSize;
        zoomEndPos = new Vector3(eP.x,eP.y,transform.position.z);
        zoomEndSize = sS;
        zoomSecond = zD;
        float f = Vector3.Distance(zoomStartPos, zoomEndPos);
        if (f != 0)
            zoomLerp = GameManager.SlerpLerp(Vector3.Distance(transform.position, zoomEndPos) / f);
        else
            zoomLerp = 0;
    }

	public void StartShaking(float sA, float sD, float sF){
		shakeAmount = sA;
		shakeFade = sF;
		shakeMaxDuration = sD;
		lerper = 0;
		shake = sA;
	}
	
	void FixedUpdate () {
        if (zoomLerp > 0) {
            zoomLerp = Mathf.Max(zoomLerp - Time.deltaTime / zoomSecond, 0);
            transform.position = Vector3.Slerp(zoomEndPos, zoomStartPos, zoomLerp);
            camComp.orthographicSize = Mathf.Lerp(zoomEndSize, zoomStartSize, GameManager.SlerpLerp(zoomLerp));
        }

        if (shake > 0) {
			transform.position -= shakeVector;
			if (shakeMaxDuration == 0){
				lerper += Time.deltaTime/shakeFade;
				shake = Mathf.Lerp (shakeAmount,0,lerper);
			}
			else
				shakeMaxDuration = Mathf.Max (shakeMaxDuration-Time.deltaTime,0);
			shakeVector = new Vector3(Random.Range(-shake,shake),Random.Range(-shake,shake),0);
			transform.position += shakeVector;
		}
	}
}
