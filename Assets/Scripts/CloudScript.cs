﻿using UnityEngine;
using System.Collections;

public class CloudScript : MonoBehaviour {

    public Vector3 direction;
    public float speed;
    Vector2 borders;
    float startSpeed;
    Vector3 startScale;

    void Start() {
        startSpeed = speed;
        startScale = transform.localScale;
        direction = direction.normalized;
        Vector2 size = GetComponent<BoxCollider2D>().size;
        borders = new Vector2((10.0f + size.x * transform.localScale.x / 2.0f), (6.0f + size.y * transform.localScale.y / 2.0f));
        transform.position = new Vector3(Random.Range(-borders.x, borders.x), Random.Range(-borders.y, borders.y), transform.position.z);
    }

    void Update() {
        transform.position += direction * speed * Time.deltaTime;

        if (direction.x != 0) {
            if ((Mathf.Sign(direction.x) == -1 && transform.position.x < -borders.x) || (Mathf.Sign(direction.x) == 1 && transform.position.x > borders.x)) {
                Randomize();
                transform.position = new Vector3(-transform.position.x, Random.Range(-5, 5), transform.position.z);
            }

            if ((Mathf.Sign(direction.y) == -1 && transform.position.y < -borders.y) || (Mathf.Sign(direction.y) == 1 && transform.position.y > borders.y)) {
                Randomize();
                transform.position = new Vector3(Random.Range(-9 ,9), -transform.position.y, transform.position.z);
            }
        }
    }

    void Randomize() {
        transform.localScale = startScale * Random.Range(0.9f, 1.1f);
        Vector2 size = GetComponent<BoxCollider2D>().size;
        borders = new Vector2((10.0f + size.x * transform.localScale.x / 2.0f), (6.0f + size.y * transform.localScale.y / 2.0f));
        speed = Random.Range(0.7f, 1.3f) * startSpeed;
    }
}
