﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndScript : MonoBehaviour {

    int currentKid = 0;
    Text currentText;
    Image currentImage;
    bool fadingIn = true;
    float alpha = 0;
    bool release = true;

	void Start () {
        GetCurrent();
	}
	
    void GetCurrent() {
        transform.GetChild(currentKid).gameObject.SetActive(true);
        currentText = transform.GetChild(currentKid).GetComponentInChildren<Text>();
        currentImage= transform.GetChild(currentKid).GetComponentInChildren<Image>();
        SetAlpha();
    }

    void SetAlpha() {
        currentText.color = (currentKid == 5) ? new Color(1, 1, 1, alpha) : new Color(0, 0, 0, alpha);
        if (currentImage != null)
            currentImage.color = new Color(1, 1, 1, alpha);
    }

	void Update () {
	    if (fadingIn) {
            alpha = Mathf.Min(1, alpha + Time.deltaTime);

            if (alpha == 1 && Input.touchCount != 0 && release) {
                release = false;
                fadingIn = false;
            }
        } else {
            alpha = Mathf.Max(0, alpha - Time.deltaTime);

            if (alpha == 0) {
                fadingIn = true;
                transform.GetChild(currentKid).gameObject.SetActive(false);
                currentKid++;
                if (currentKid == transform.childCount)
                    Application.LoadLevel(0);
                else
                    GetCurrent();
            }
        }

        if (!release && Input.touchCount == 0)
            release = true;

        SetAlpha();
	}
}
