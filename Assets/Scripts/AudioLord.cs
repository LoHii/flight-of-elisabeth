﻿using UnityEngine;
using System.Collections;

public class AudioLord : MonoBehaviour {

	public static AudioLord lord;
	[HideInInspector]
	public Transform holder;

	void Start () {
		if (lord != null)
			Debug.LogError ("Multiple AudioLords");
		else
			lord = this;

		for (int i = 0; i < transform.childCount; i++)
			transform.GetChild (i).tag = "AudioObject";
		holder = new GameObject("Current Sounds").transform;
	}

	public static void Play(string name){
        GameObject go = lord.transform.Find(name).gameObject;
		AudioSource audio = Instantiate (go).GetComponent<AudioSource> ();
		audio.Play ();
		audio.transform.SetParent (lord.holder);
		Destroy (audio.gameObject, audio.clip.length);
	}

	public static AudioSource PlayLoop(string name){
		GameObject go = GameObject.Find(lord.name + "/"+ name);
		AudioSource audio = Instantiate (go).GetComponent<AudioSource> ();
		audio.transform.SetParent (lord.holder);
		audio.Play ();
		return audio;
	}

	public static void DestroyLoop(AudioSource audio, float fadeTime){
        if (audio.gameObject != null) {
            GameObject go = audio.gameObject;
            AudioFader af = go.AddComponent<AudioFader>();
            af.fadeTime = fadeTime;
        }
	}
}
