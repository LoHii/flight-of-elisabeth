﻿using UnityEngine;
using System.Collections;

public class SpriteBlacker : MonoBehaviour {

    SpriteRenderer rend;
    Color orig;
    public float staySeconds;
    float stayTimer;
    public float fadeSeconds;
    float fadeTimer;
    GameObject black;

    void Start() {
        black = new GameObject("SpriteBlacker");
        black.transform.position = transform.position;
        black.transform.SetParent(transform,true);
        rend = GetComponent<SpriteRenderer>();
        SpriteRenderer copy = black.AddComponent<SpriteRenderer>();
        copy.sprite = rend.sprite;
        copy.sortingOrder = 1000;
        rend = copy;
        rend.color = Color.black;
    }

    void Update() {
        if (staySeconds > stayTimer)
            stayTimer += Time.deltaTime;
        else if (fadeSeconds > fadeTimer) {
            fadeTimer += Time.deltaTime;
            rend.color = new Color(0,0,0,1-fadeTimer/fadeSeconds);
        } else {
            Destroy(black);
            Destroy(this);
        }
    }
}
